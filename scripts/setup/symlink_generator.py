from os import listdir
from os import path

from os.path import isfile, join

# Creates symlinks to each file in the symlink_folder in the destination
def create_symlinks(destination, symlink_folder):

    # Get all files in the symlink_folder
    files = [f for f in listdir(symlink_folder) if isfile(join(symlink_folder, f))]
    
    if len(files) == 0:
        print('No files found in directory' + symlink_folder)
        return

    # Foreach file, create a symlink (making it hidden)
    for filename in files:
        if filename == '.' or filename == '..':
            continue

        # Create the symlink path
        symlink = path.join(destination, '.', filename)

        # Create the target path
        target = path.join(symlink_folder, filename)

        # Create the symlink
        os.symlink(target, symlink)
