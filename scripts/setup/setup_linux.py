from symlink_generator import create_symlinks
from os.path import expanduser

# Get the appropriate folders
home = expanduser("~")

bashrc = os.path.abspath('../bashrc')
git    = os.path.abspath('../git')
vim    = os.path.abspath('../vim')

# Create symlinks
create_symlinks(home, bashrc)
create_symlinks(home, git)
create_symlinks(home, vim)
