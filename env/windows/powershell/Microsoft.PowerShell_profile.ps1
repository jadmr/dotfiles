# Set me under c:/Users/User/Documents/WindowsPowerShell

Set-Location C:/Users/User

Set-Alias -Name cat -Value Get-Content -Option AllScope
Set-Alias -Name which -Value get-command
Set-Alias -Name man -Value Get-Help -Option AllScope
Set-Alias -Name nautilus -Value ii
Set-Alias -Name poweroff -Value Stop-Computer
Set-Alias -Name touch -Value New-Item
Set-Alias sublime_text 'C:/Program Files/Sublime Text 3/sublime_text.exe''

