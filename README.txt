The intent is that all shell environments contain a BASHRC and VIMRC file located in the HOME directory.
These two files are symbolic links to those contained with the 'bash' and 'vim' folder in this repo.
Thus, all environments share the *same* bashrc and vimrc.

BASHRC and VIMRC will not define anything.
Instead, they will check if a specifed file exists at $HOME/.bashenv.
If the file exists, it will be loaded.

The different environments (Linux, Android, Windows) will provide their own versions of these files as needed.
For example...

1) Since there is no Java runtime accessible within Termux, an Android environment will not provide
a Java file at '~/.bashenv/java', and these settings will be skipped.

2) Since Android wants its own specific version of PATH different from that in a Linux environment, an Android
environment will provide its own '~/.bashenv/path' file

All of the files within ~/.bashenv are symlinks into the specific file of each environment needed.
